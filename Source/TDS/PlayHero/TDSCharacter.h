// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "TDS/PlayHero/TDSInventoryComponent.h"
#include "TDS/PlayHero/TDSCharacterHealthComponent.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "TDS/StateEffects/TDS_StateEffect.h"

#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	// Inputs
	void InputAxisX(float Value);
	void InputAxisY(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();


	// Inventory Func
	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();

	//Ability func
	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}

	// Input flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	bool SprintRunEnabled = false;
	bool WalkEnabled = false;
	bool AimEnabled = false;

	bool bIsAlive = true;

	EMovementState MovementState = EMovementState::Run_State;

	AWeaponDefault* CurrentWeapon = nullptr;

	UDecalComponent* CurrentCursor = nullptr;

	TArray<UTDS_StateEffect*> Effects;

	FName NameBoneToAttachedEffect = TEXT("None");
	FVector LocationOffsetToAttachedEffect = FVector(0);

	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
	void CharDead();
	void EnableRagdoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:
	ATDSCharacter();

	FTimerHandle TimerHandle_RagdollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	class UTDSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	class UTDSCharacterHealthComponent* HealthComponent;

	// Cursor
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;



	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadAnims;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Abilities")
		TSubclassOf<UTDS_StateEffect> AbilityEffect;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float Stamina = 100.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float MaxStamina = 100.f;

	// For spint movement direction
	float ForwardX = 0.0f;
	float ForwardY = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
		FString ButtonSprint;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:

	// Tick Func
	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION()
	void StaminaTick();

	//Func
	void AttackCharEvent(bool bIsFiring);
	void CharacterUpdate();
	void ChangeMovementState();
	void ChangeMovementForwardDirectonValues();


	UFUNCTION()
	void DecreaseStamina(float value);
	UFUNCTION()
	void RealableStamina(float value);


	UFUNCTION()
	void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION()
	void TryReloadWeapon();

	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);

	void DropCurrentWeapon();
	bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess);


	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<UTDS_StateEffect*> GetCurrentEffectsOnCharacter();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetCharacterIsAlive();

	// INTERFACE
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTDS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;
	void AddEffect(UTDS_StateEffect* newEffect) override;
	USkeletalMeshComponent* GetSkeletalMeshActor() override;
	FName GetNameOfAttachedBone() override;
	FVector GetActorLocationOffset() override;
	// END INTERFACE

	//
	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();
	
	
	
};

