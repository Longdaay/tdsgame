// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TDS/StateEffects/TDS_StateEffect.h"

#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8 
{
	Aim_State		UMETA(DisplayName = "Aim State"),
	AimWalk_State	UMETA(DisplayName = "AimWalk State"),
	Walk_State		UMETA(DisplayName = "Walk State"),
	Run_State		UMETA(DisplayName = "Run State"),
	SprintRun_State	UMETA(DisplayName = "SprintRun State"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType			UMETA(DisplayName = "Rifle"),
	ShotgunType			UMETA(DisplayName = "Shotgun"),
	SniperRifleType		UMETA(DisplayName = "Pistol"),
	GrenadeLauncherType UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncherType	UMETA(DisplayName = "RocketLauncher"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeedNormal	= 300.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeedNormal	= 200.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeedNormal	= 600.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeedWalk		= 100.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
	float SprintRunSpeedRun = 800.f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	UParticleSystem* ProjectileTrailFx = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	FTransform ProjectileTrailFxOffset = FTransform();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileInitSpeed = 2000.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileMaxSpeed = 2000.0f;


	// Material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	// Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	USoundBase* HitSound = nullptr;
	// Fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	TSubclassOf<UTDS_StateEffect> Effect = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explode")
	UParticleSystem* ExploseFX = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explode")
	USoundBase* ExploseSound = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explode")
	float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explode")
	float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explode")
	float ExploseMaxDamage = 40.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explode")
	float ExploseFallofCoed = 1.0f;
	// Timer add
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Aim_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Despersion")
	float Run_StateDispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Char")
	UAnimMontage* AnimCharReloadAim = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Weapon")
	UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Weapon")
	UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim Weapon")
	UAnimMontage* AnimWeaponFire = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	UStaticMesh* DropMesh = nullptr;
	// 0.0f immediately drop
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	float DropMeshTime = -1.0;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	float DropMeshLifeTime = 5.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	FVector DropMeshImpulsDirection = FVector(0.0f);
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	float PowerImpulse = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Mesh")
	float CustomMass = 0.0f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
	int32 MaxRound = 10;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "State")
	int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Dispersion")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")
	UParticleSystem* EffectFireWeapon = nullptr;
	// if null use trace logic (TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileSetting;
	//UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Trace")
	//float WeaponDamage = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Trace")
	float DistanceTrace = 2000.f;
	// one decal on all? no
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "HitEffect ")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Anim")
	FAnimationWeaponInfo AnimWeaponInfo;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Mesh")
	FDropMeshInfo MagazineDrop;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Mesh")
	FDropMeshInfo ShellBullets;

	// inventory
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	float SwitchTimeToWeapon = 1.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Inventory")
	EWeaponType WeaponType = EWeaponType::RifleType;

};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 Round = 0;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "WeaponSlot")
	FName NameItem;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

		// Index Slot by index Array
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Cout = 100;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 MaxCout = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	// Index Slot by index Array
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropWeapon")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropWeapon")
	USkeletalMesh* WeaponSkeletalMesh = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropWeapon")
	FWeaponSlot WeaponInfo;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTDS_StateEffect>AddEffectClass, EPhysicalSurface SurfaceType);
};