// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/StateEffects/TDS_StateEffect.h"
#include "TDS/PlayHero/TDSHealthComponent.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UTDS_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();

	}
}


bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor) {
		UTDSHealthComponent* myHelthComp =  Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHelthComp)
		{
			myHelthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffects.Num() > 0)
	{
		// to do for object with interface func return offset location, 1 func , name bones, 
		//done // toDo random init effect wiht available array particles (for)
		int32 Rnd = FMath::RandHelper(ParticleEffects.Num());
		if (ParticleEffects.IsValidIndex(Rnd))
		{
			FName NameBoneToAttached = NameBoneHit;
			FVector AttachedOffsetLocation = FVector(0);
			USkeletalMeshComponent* AttachedSkeletalMesh = nullptr;
			ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
			if (myInterface)
			{
				AttachedSkeletalMesh = myInterface->GetSkeletalMeshActor();
				NameBoneToAttached = myInterface->GetNameOfAttachedBone();
				AttachedOffsetLocation = myInterface->GetActorLocationOffset();

				if (AttachedSkeletalMesh)
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffects[Rnd], AttachedSkeletalMesh, NameBoneToAttached, AttachedOffsetLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				else
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffects[Rnd], myActor->GetRootComponent(), TEXT("None"), AttachedOffsetLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			}
			else
			{
				USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
				if (myMesh)
				{
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffects[Rnd], myMesh, NameBoneHit, AttachedOffsetLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				}
				else
				{
					ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffects[Rnd], myActor->GetRootComponent(), TEXT("None"), AttachedOffsetLocation, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				}
			}
		}
	}
	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
	}
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor) {
		UTDSHealthComponent* myHelthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHelthComp)
		{
			myHelthComp->ChangeHealthValue(Power);
		}
	}
}
